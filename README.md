# Darktable Keybindings reworked

I wrote [an article about Darktable from a former Lightroom users perspective](https://mathiashueber.com/an-introduction-to-darktable-for-lightroom-user/). In the course of this article, I have created a new set of Darktable 3.0.2 keyboard shortcuts. The result is from my perspective a bit more intuative to former Adobe Lightroom users.